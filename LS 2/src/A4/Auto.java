package A4;
// Autobeispiel aus Lehrervortrag vom 16.03.2022

public class Auto {
	
	private String hersteller;
	private String modell;
	
	public Auto (String hersteller, String modell) {
		this.hersteller = hersteller;
		this.modell = modell;
	}
	
	public void fahre(double distanz) {
		
	}
	public double tanke(double menge) {
		
		return menge * 2.90; // 2,90 Euro
	}
	
	// Verwaltung:
	public String getHersteller() {
		return this.hersteller;
	}
	public String getModell() {
		return this.modell;
	}
	
	public String toString() {
		return this.hersteller 
				+ " " + this.modell;
	}
}
