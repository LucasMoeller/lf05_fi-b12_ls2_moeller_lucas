package �bungen;

/**
 * 
 * @author Lucas M�ller
 * @version 1.0
 * 
 *
 */

public class Person {
	String name;
	int alter;
	
	
	public Person(String name, int alter) {
		this.alter = alter;
		this.name = name;
	}
	
	@Override
	public String toString() {
		return "'" + name +"':  " + name + ", " + alter;
	}
	


	public String getName() {
		return name;
	}
	
	
	/**
	 * @param name
	 * @return void
	*/
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * 
	 * @return alter
	 */
	public int getAlter() {
		return alter;
	}
	
	
	/**
	 * 
	 * @param alter
	 * @return void
	 */
	public void setAlter(int alter) {
		this.alter = alter;
	}
	
}
