package A4_1;

/**
 * 
 * @author Lucas M�ller
 *
 */

import java.util.ArrayList;

public class RaumschiffeTests {

	public static void main(String[] args) {
		

		//  ------ A4.1.1: Quellcode verwenden ------------------------------------
		
		Raumschiff klingonen = new Raumschiff(	"IKS Hegh'ta", 1, 100, 100,100, 100,2);
		klingonen.addLadung(new Ladung("Ferengi Schneckensaft", 200));
		klingonen.addLadung(new Ladung("Bat'leth Klingonen-Schwert", 200));
		
		Raumschiff romulaner = new Raumschiff("IRW Khazara", 2, 100, 100, 100, 100, 2);
		romulaner.addLadung(new Ladung("Borg-Schrott", 5));
		romulaner.addLadung(new Ladung("Rote Materie", 2));
		romulaner.addLadung(new Ladung("Plasma-Waffe", 50));
		
		Raumschiff vulkanier = new Raumschiff("Ni'Var", 0, 80,80,50,100, 5);
		vulkanier.addLadung(new Ladung("Forschungssonde", 35));
		vulkanier.addLadung(new Ladung("Photonentorpedo", 3));
		
		System.out.println("A4.1.1,  \nLadungsverzeichnisse: "
			+ "\nder Klingonen:\t"+ klingonen.getLadungsverzeichnis()
			+ "\nder Romulaner\t" + romulaner.getLadungsverzeichnis()
			+ "\nder Vulkanier\t" + vulkanier.getLadungsverzeichnis());
		//  ------------------------------------------------------------------------
		
		//  ------ A 4.2.1: Quellcode verwenden *** ---  abgeschlossen ------------------------
		
		System.out.println("\n\nA 4.2.1 ***");
		
		
		klingonen.photonentorpedoSchiessen(romulaner);		
		
		romulaner.phaserkanoneSchiessen(klingonen);
		
		vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch.");
		
		klingonen.zustandRaumschiff();
		klingonen.ladungsverzeichnisAusgeben();
		
		vulkanier.reparaturDurchfuehren(true, true, true, 5);
		
		vulkanier.photonentorpedosLaden(3);
		vulkanier.ladungsverzeichnisAufraeumen();
		
		klingonen.photonentorpedoSchiessen(romulaner);
		klingonen.photonentorpedoSchiessen(romulaner);
		
		klingonen.zustandRaumschiff();
		klingonen.ladungsverzeichnisAusgeben();
		romulaner.zustandRaumschiff();
		romulaner.ladungsverzeichnisAusgeben();
		vulkanier.zustandRaumschiff();
		vulkanier.ladungsverzeichnisAusgeben();
		
		System.out.println("\n\nBroadcastKommunikatoren: (klingonen romulaner, vulkanier)");
		System.out.println(klingonen.eintraegeLogbuchZurueckgeben());
		System.out.println(romulaner.eintraegeLogbuchZurueckgeben());
		System.out.println(vulkanier.eintraegeLogbuchZurueckgeben());
		
		
		
		
	//  ------------------------------------------------------------------------	
		
	// Eigene, inoffizielle Tests
	/*
	klingonen.treffer(vulkanier);
	System.out.println(vulkanier.getSchildeInProzent());
	klingonen.treffer(vulkanier);
	System.out.println(vulkanier.getSchildeInProzent());
	klingonen.treffer(vulkanier);
	*/
		
	
	/*
	vulkanier.photonentorpedosLaden(3);
	System.out.println(vulkanier.getLadungsverzeichnis());
	vulkanier.ladungsverzeichnisAufraeumen();
	System.out.println(vulkanier.getLadungsverzeichnis());
	vulkanier.zustandRaumschiff();
	
	vulkanier.setSchildeInProzent(50);
	vulkanier.zustandRaumschiff();
	vulkanier.reparaturDurchfuehren(true,true,true, 3);
	vulkanier.zustandRaumschiff();
	vulkanier.reparaturDurchfuehren(true,true,true, 3);
	vulkanier.zustandRaumschiff();
	vulkanier.photonentorpedosLaden(3);
	vulkanier.zustandRaumschiff();
	*/
	//  ------------------------------------------------------------------------
	}

}
