package A4_1;

/**
 * 
 * @author Lucas M�ller
 *
 */

import java.util.ArrayList;

public class Raumschiff {
	private String schiffname;
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private ArrayList<String> broadcastKommunikator;
	private ArrayList<Ladung> ladungsverzeichnis;
	
	public Raumschiff(	String schiffname, 				int photonentorpedoAnzahl,
						int energieversorgungInProzent, int schildeInProzent,
						int huelleInProzent,			int lebenserhaltungssystemeInProzent,
						int androidenAnzahl
						) 
	{
		this.schiffname = schiffname;
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.broadcastKommunikator = new ArrayList<String>();
		this.ladungsverzeichnis = new ArrayList<Ladung>();
	}
	
	
	/* (aktive) Methoden
	Die Methoden sind nach dem offiziellem L�ser (OOD) gegliedert, bei folgender URL:
 		https://moodle.oszimt.de/pluginfile.php/399003/assignfeedback_solutionsheet/solutionsheet/0/Aufgabe%204%20Loeser_KlassendiagrammOOD_StarTrek.pdf?forcedownload=1
	
	Parameter und R�ckgabewerte entsprechenden den genauen Angaben im OOD-Klassendiagramm.
	Die Verwaltungsmethoden habe ich unten angeh�ngt.
	*/
	// TODO: aktueller Arbeitsstand 
	//		A 5.1 JavaDoc-Dokumentation

	
	
	
	// bearbeitet in A 4.1	
	/**
	 * 
	 * @param l hinzuzuf�gende Ladung
	 */
	public void addLadung(Ladung l) {
		this.ladungsverzeichnis.add(l);
	}
	
	// bearbeitet in A 4.2 ***		(Schritt 3) - fertig
	/**
	 * 
	 * @param r das Ziel-Raumschiff
	 */
	public void photonentorpedoSchiessen(Raumschiff r){		// r ist das Zielraumschiff.
		if (this.photonentorpedoAnzahl == 0){
			this.nachrichtAnAlle("-=*Click*=-");
		}	
		else {
			this.photonentorpedoAnzahl -= 1;
			this.nachrichtAnAlle("Photonentorpedo abgeschossen.");
			this.treffer(r);				
		}
	}
	
	
	// bearbeitet in A 4.2 ***		(Schritt 4) - fertig
	/**
	 * @param r das Ziel-Raumschiff
	 */
	public void phaserkanoneSchiessen(Raumschiff r){
		if(this.energieversorgungInProzent < 50) {
			this.nachrichtAnAlle("-=*Click*=-");
		}
		else {
			this.energieversorgungInProzent -= 50;
			this.nachrichtAnAlle("Phaserkanone abgeschossen.");
			this.treffer(r);
		}
		
	}
	
	
	// bearbeitet in A 4.2 ***		(Schritt 5 & Schritt 8) - fertig
	/**
	 * @param r das Ziel-Raumschiff
	 */
	public void treffer(Raumschiff r){
		System.out.println(r.getSchiffname() + " wurde getroffen!");
		
		// Zweite Teilbearbeitung (10 Punkte)
		r.setSchildeInProzent(r.getSchildeInProzent()-50);
		
		if(r.getSchildeInProzent() <= 0) {
			r.setEnergieversorgungInProzent(r.getEnergieversorgungInProzent()-50);
			r.setHuelleInProzent(r.getHuelleInProzent()-50);
			
		}
		if(r.getHuelleInProzent() <= 0) {
			r.setLebenserhaltungssystemeInProzent(0);
			this.nachrichtAnAlle("Lebenserhaltungssysteme des Schiffs " + r.getSchiffname() + "wurden vernichtet.");
		}
		
	}
	
	// bearbeitet in A 4.2 ***		(Schritt 6) - fertig 
	/**
	 * @param message "die Nachricht als String"
	 */
	public void nachrichtAnAlle(String message){
		this.broadcastKommunikator.add(message);
		System.out.println(this.getSchiffname() +": " + message);
		
		
	}
		
	// bearbeitet in A 4.2 ***		(Schritt 7) - fertig
	/** 
	 * @return broadcastKommunikator , Gibt alle Eint�ge der ArrayList broadcastKommunikator zur�ck.
	 */
	public ArrayList<String> eintraegeLogbuchZurueckgeben() {
		// blo� eine einfache R�ckgabe
		return this.broadcastKommunikator;
	}
	
	
	// bearbeitet in A 4.2 ***		(Schritt 9) - fertig - funktioniert
	/**
	 * @param anzahlTorpedos , als int
	 */
	public void photonentorpedosLaden(int anzahlTorpedos){
		boolean torpedosImLadungsverzeichnis = false;
		int stelle = 0;
		
		// Bestimme, ob Torpedos als Ladung vorhanden sind.
		for(int i = 0; i < this.getLadungsverzeichnis().size(); i ++) {
			torpedosImLadungsverzeichnis = this.getLadungsverzeichnis().get(i).getBezeichung() == "Photonentorpedo";
			stelle = i;
			if(torpedosImLadungsverzeichnis)
				break;
		}
		
		// Entscheide, entsprechend des Befunds:
		if(!torpedosImLadungsverzeichnis) {
			System.out.println("Keine Photonentorpedos gefunden!");
			this.nachrichtAnAlle("-=*Click*=-");
		}
		else {
			Ladung objTorpedosLadung = this.getLadungsverzeichnis().get(stelle);
			int menge =  objTorpedosLadung.getMenge();

				
			int eingesetzt;
			if(anzahlTorpedos > menge) { 
				// Lade die vorhandene Anzahl Torpedos
				this.setPhotonentorpedoAnzahl(this.getPhotonentorpedoAnzahl() + menge);
				eingesetzt = menge;
			}
			else {
				 // Lade die gew�nschte Anzahl Torpedos
				this.setPhotonentorpedoAnzahl( this.getPhotonentorpedoAnzahl() + anzahlTorpedos);
				eingesetzt = anzahlTorpedos;
			}
			objTorpedosLadung.setMenge(objTorpedosLadung.getMenge() - eingesetzt);
			System.out.println(eingesetzt +" Photonentorpedo(s) eingesetzt.");
		}
	}
	
	
	// bearbeitet in A 4.2 ***		(Schritt 11) 
	/**
	 * 
	 * @param schutzschilde in Prozent
	 * @param energieversorgung in Prozent
	 * @param schiffshuelle in Prozent
	 * @param anzahlDroiden als int
	 */
	public void reparaturDurchfuehren(boolean schutzschilde, boolean energieversorgung, boolean schiffshuelle, int anzahlDroiden){
		
		int eingesetzt = anzahlDroiden;
		if(anzahlDroiden > this.androidenAnzahl) {
			eingesetzt = this.androidenAnzahl;
		}
		
		int anzStrukturen = 0;			// anschlie�end maximal 3
		if(schutzschilde) 
			anzStrukturen += 1;
		if(energieversorgung) 
			anzStrukturen += 1;
		if(schiffshuelle) 
			anzStrukturen += 1;
		
		// Berechne drei Summanden f�r die Prozents�tze, 
		// welche jeweils durch den Zufall beeinflusst sind.
		int[] prozents�tze = new int[3];
		for(int i=0; i<3; i++) {
			int rand = (int) (Math.random()* 100);  			// Zufallszahl zwischen 0 und 100
			prozents�tze[i] =  rand* eingesetzt / anzStrukturen;
		}
		
		
		this.schildeInProzent 			+= schutzschilde? 		prozents�tze[0]		: 0;
		this.energieversorgungInProzent += energieversorgung?	prozents�tze[1] 	: 0;		
		this.huelleInProzent 			+= schiffshuelle? 		prozents�tze[2]		: 0;	
		
		//Dieses Inkrement entspricht:
		// wenn bool == true 
		// 		addiere Prozentsatz auf aktuellen Wert, 
		// sonst 
		//		belasse ihn bei seinem aktuellen Wert.
		
		
		/*
		// Verhindere einen �berlauf der Werte �BER 100% :
		if( this.schildeInProzent > 100)
			this.schildeInProzent = 100;						
		
		if( this.energieversorgungInProzent > 100) 
			this.energieversorgungInProzent = 100;
		
		if( this.huelleInProzent > 100) 
			this.huelleInProzent = 100;
		*/
		
		
	}
	
	
	// bearbeitet in A 4.2 ***		(Schritt 1) - fertig
	
	public void zustandRaumschiff() {
		System.out.println("\nZustand des Raumschiffs:\n" + this.toString());
	}
	/**
	 * @return Gibt die Zust�nde aller Attribute als zusammengesetzen String aus.
	 */
	public String toString() {
		return 
				"schiffname : " 						+ this.schiffname						+"\n"
				+ "photonentorpedoAnzahl : " 			+ this.photonentorpedoAnzahl			+"\n"
				+ "energieversorgungInProzent : " 		+ this.energieversorgungInProzent		+"\n"
				+ "schildeInProzent : " 				+ this.schildeInProzent					+"\n"
				+ "huelleInProzent : " 					+ this.huelleInProzent					+"\n"
				+ "lebenserhaltungssystemeInProzent : "	+ this.lebenserhaltungssystemeInProzent	+"\n"
				+ "androidenAnzahl : " 					+ this.androidenAnzahl					+"\n"
				+ "broadcastKommunikator : " 			+ this.broadcastKommunikator			+"\n"
				+ "ladungsverzeichnis : "				+ this.ladungsverzeichnis				+"\n";
	}
	
	
	// bearbeitet in A 4.2 ***		(Schritt 2) - fertig
	public void ladungsverzeichnisAusgeben() {
		System.out.println("\nLadungen des Schiffs "+this.schiffname + " :");
		System.out.println(this.ladungsverzeichnis);
		// Das Ladungsverzeichnis wird als ArrayList ausgegeben. 
		// Jedes Element (des Typs Ladung) wird als String im Format nach toString() ausgegeben. 
	
	}
	
	// bearbeitet in A 4.2 ***		(Schritt 10) - fertig
	public void ladungsverzeichnisAufraeumen() {
		
		for(int i = 0; i < ladungsverzeichnis.size(); i ++) {
			if (ladungsverzeichnis.get(i).getMenge() == 0) {
				ladungsverzeichnis.remove(i);
			}
		}
		
		
	}
	
	
	
	
		
	
	// Verwaltungsmethoden zu allen Attributen
	public String getSchiffname() {
		return schiffname;
	}
	
	public void setSchiffname(String schiffname) {
		this.schiffname = schiffname;
	}

	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}

	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}

	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}

	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}

	public int getSchildeInProzent() {
		return schildeInProzent;
	}

	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}

	public int getHuelleInProzent() {
		return huelleInProzent;
	}

	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}

	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}

	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}

	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}

	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

	public ArrayList<String> getBroadcastKommunikator() {
		return broadcastKommunikator;
	}

	public void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
		this.broadcastKommunikator = broadcastKommunikator;
	}

	public ArrayList<Ladung> getLadungsverzeichnis() {
		return ladungsverzeichnis;
	}

	public void setLadungsverzeichnis(ArrayList<Ladung> ladungsverzeichnis) {
		this.ladungsverzeichnis = ladungsverzeichnis;
	}
}
