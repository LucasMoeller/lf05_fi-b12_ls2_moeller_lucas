package A4_1;

/**
 * 
 * @author Lucas M�ller
 *
 */

public class Ladung {
	
	private String bezeichnung;
	private int menge;
	
	
	public Ladung(String bezeichnung, int menge) {
		this.bezeichnung = bezeichnung;
		this.menge = menge;
		
	}
	
	/**
	 * 
	 * @return String bezeichnung
	 */
	public String getBezeichung() {
		return this.bezeichnung;
	}
	/**
	 * 
	 * @return int menge
	 */
	public int getMenge() {
		return this.menge;
	}
	/**
	 * 
	 * @param menge als Parameter
	 */
	public void setMenge(int menge) {
		this.menge = menge;
	}
	
	/**
	 * @return String, komplette Beschreibung der Ladung
	 */
	@Override
	public String toString() {
		return "Ladung: " + "|" + this.bezeichnung +":" +this.menge + "|";
	}
}
